﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter n");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter m");
            int m = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Matrix matrix = new Matrix(n, m);
            matrix.setMatrix();
            matrix.getMatrix();
            Console.Write("Number of first UI column - ");
            Console.WriteLine(matrix.getNumberOfFirstUIColumn());
            Console.WriteLine("---------------");
            Console.WriteLine("Accending rows by same elements");
            matrix.AccendingRows();
            Console.WriteLine("---------------");
            Console.WriteLine("Sum of rows with negative element");
            matrix.sumOfRows();
            Console.WriteLine("---------------");
            Console.WriteLine("Saddle Point:");
            matrix.SaddlePoint();
            Console.WriteLine("---------------");
            Console.WriteLine("Matrix 8x8");
            Console.WriteLine("---------------");
            Matrix matrix8 = new Matrix(8, 8);
            matrix8.setMatrix();
            Console.WriteLine("---------------");
            Console.WriteLine("Same column and row");
            matrix8.sameRowAndColumn();
            Console.WriteLine("---------------");
            Console.WriteLine("Sum of rows with negative element");
            matrix8.sumOfRows();
            Console.ReadKey();
        }
    }
}
