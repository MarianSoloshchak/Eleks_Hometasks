﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask01
{
    class Matrix
    {
        public int[,] mas;
        public int n;
        public int m;
        public Matrix(int _n, int _m)
        {
            n = _n;
            m = _m;
            mas = new int[n, m];
        }
        public void setMatrix()
        {
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    Console.Write(string.Format("input element [{0},{1}] - ", i, j));
                    mas[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
        }
        public void getMatrix()
        {
            Console.WriteLine("-------------------------------");
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    Console.Write(" " + mas[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("-------------------------------");
        }
        //Task2
        public int getNumberOfFirstUIColumn()
        {

            bool flag = true;
            for (int i = 0; i < m; ++i)
            {
                flag = true;
                for (int j = 0; j < n; ++j)
                {
                    if (mas[j, i] < 0)
                    {
                        flag = false;
                    }
                }
                if (flag)
                {
                    return i;
                }
            }
            return -1;
        }
        public void AccendingRows()
        {
            List<int>[] matr = new List<int>[n];
            for (int i = 0; i < n; i++)
            {
                matr[i] = new List<int>();
            }
            for(int i=0;i<n;++i)
            {
                for(int j=0;j<m;++j)
                {
                    matr[i].Add(mas[i, j]);
                }
            }
            for(int i =0;i<n;++i)
            {
                int maxCount = 0;
                int counter = 0;
                foreach(var obj in matr[i])
                {
                    foreach(var obj1 in matr[i])
                    {
                        if(obj==obj1)
                        {
                            counter++;
                        }
                    }
                    if (counter > maxCount)
                    {
                        maxCount = counter;
                    }
                    counter = 0;
                }
                matr[i].Add(maxCount);
            }
            for(int i=0;i<n;++i)
            {
                for(int j=0;j<n;++j)
                {
                    if(matr[i][m] < matr[j][m])
                    {
                        List<int> helpNode = matr[i];
                        matr[i] = matr[j];
                        matr[j] = helpNode;
                    }
                }
            }
            for(int i = 0;i<n;++i)
            {
                for(int j=0;j<m;++j)
                {
                    Console.Write(" " + matr[i][j] + " ");
                }
                Console.WriteLine();
            }
        }
        //Task3
        public void sumOfRows()
        {
            int[] result = new int[n];
            bool flag = false;
            int sum = 0;
            for(int i=0;i<n;++i)
            {
                for(int j=0;j<m;++j)
                {
                    if (mas[i, j] < 0)
                    {
                        flag = true;
                    }
                }
                if(flag)
                {
                    for(int j=0;j<m;++j)
                    {
                        sum += mas[i, j];
                    }
                    
                }
                result[i] = sum;
                sum = 0;
                flag = false;
            }
            for(int i=0;i<n;++i)
            {
                Console.WriteLine(result[i]);
            }
        }
        public void SaddlePoint()
        {
            int[] row = new int[m];
            int[] col = new int[n];
            for (int i=0;i<n;++i)
            {
                
                for(int j=0;j<m;++j)
                {
                    row[j] = mas[i, j];
                }
                int indexMin = 0;
                int min=row[0];
                for(int j =1;j<m;++j)
                {
                    if(row[j]<min)
                    {
                        min = row[j];
                        indexMin = j;
                    }
                }
                int indexMax = 0;
                for(int k=0;k<n;++k)
                {
                    col[k] = mas[k, indexMin];
                }
                int max = col[0];
                for (int j = 1; j < n; ++j)
                {
                    if (col[j] > max)
                    {
                        max = col[j];
                        indexMax = j;
                    }
                }
                if(indexMax==i)
                {
                    Console.WriteLine("Saddle point in["+indexMax+";"+indexMin+"]");
                }
            }
        }
        //Task4
        public void sameRowAndColumn()
        {
            int[] row = new int[n];
            int[] col = new int[n];
            bool flag = true;
            for(int i=0;i<n;++i)
            {
                flag = true;
                for(int j=0;j<n;++j)
                {
                    row[j] = mas[i, j];
                    col[j] = mas[j, i];
                }
                for(int j=0;j<n;++j)
                {
                    if(row[j]!=col[j])
                    {
                        flag = false;
                    }
                }
                if(flag)
                {
                    Console.WriteLine("k="+i);
                }

            }
        }
    }
}
