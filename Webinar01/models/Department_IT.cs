﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{
    [XmlInclude(typeof(Department_IT))]
    public class Department_IT : Departments
    {

        public Department_IT()
        {
            Name = "Department of IT";
        }
        public Department_IT(List<Book> b)
        {
            Name = "Department of IT";
            Books = b;
        }
        public override string ToString()
        {
            return Name + ": Count of Books - " + this.Books.Count;
        }
        public override int BooksCounter()
        {
            return this.Books.Count;
        }
    }
}
