﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{
    [XmlInclude(type: typeof(Book))]
    public class Book : IComparable
    {
        public string Name { get; set; }
        public Author Author { get; set; }
        public int CountOfPages { get; set; }
        public Book()
        {
            Name = "Default Name";
            CountOfPages = 0;
            Author = new Author();
        }
        public Book(string name, Author author, int pages)
        {
            Name = name;
            CountOfPages = pages;
            Author = author.setBook(this);
        }
        public override string ToString()
        {
            return Name + " (" + Author.Name + "), Count of pages - " + CountOfPages + "/n";
        }
        public int CompareTo(object obj)
        {
            Book book = obj as Book;
            return this.CountOfPages.CompareTo(book.CountOfPages);
        }
    }
}
