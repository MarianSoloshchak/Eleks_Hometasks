﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{
    [XmlInclude(typeof(Department_WorldLiterature))]
    public class Department_WorldLiterature : Departments
    {
        
        public Department_WorldLiterature()
        {
            Name = "Department of World Literature";
        }
        public Department_WorldLiterature(List<Book> b)
        {
            Name = "Department of World Literature";
            Books = b;
        }
        public override string ToString()
        {
            return Name + ": Count of Books - " + this.Books.Count;
        }
        public override int BooksCounter()
        {
            return this.Books.Count;
        }
    }
}
