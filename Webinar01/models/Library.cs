﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Webinar01
{
   [XmlInclude(typeof(Library))]
    public class Library : ICountingBooks
    {
        public string Name { get; set; }
        public List<Departments> Departments { get; set; }
        public List<Author> Authors { get; set; }
        public Library()
        {

            Name = "Default Library";
            Departments = new List<Departments>();
            Authors = new List<Author>();
          
        }
        public Library(string name)
        {
            Name = name;
            Departments = new List<Departments>
            {
                new Department_Fantastic(),
                new Department_IT(),
                new Department_UkrainianLiterature(),
                new Department_WorldLiterature()
            };
            Authors = new List<Author> { new Author("Steven King"),
                                         new Author("Taras Shevchenko"),
                                         new Author("Ivan Franko"),
                                         new Author("Den Braun") };
            foreach (Departments obj in Departments)
            {
                obj.Books = createBooks();
            }

        }
        public int BooksCounter()
        {
            int counter = 0;
            foreach (var obj in this.Departments)
            {
                foreach (var book in obj.Books)
                {
                     counter++;
                }
            }
            return counter;
        }
        public List<Book> createBooks()
        {
            List<Book> list = new List<Book>();
            int counter = 0;
            for (int i = 0; i < 10; ++i,counter++)
            {
                if(counter==4)
                {
                    counter = 0;
                }
                Book b = new Book(string.Format("Book №{0}", i), Authors[counter], i * 10 / 2 + 3);
                list.Add(b);

            }
            return list;
        }

        public void toXML()
        {
            
            XmlSerializer serializer = new XmlSerializer(typeof(Departments));
            using (StreamWriter writer = new StreamWriter("libraryData.xml"))
            {
                serializer.Serialize(writer, this.Departments[0]);
            }
            //Departments dep0 = this.Departments[0] as Departments;
            //Departments dep1 = this.Departments[1] as Departments;
            //Departments dep2 = this.Departments[2] as Departments;
            //Departments dep3 = this.Departments[3] as Departments;
            //XDocument doc = new XDocument(new XElement(this.Name,
            //                                                new XElement("Department",
            //                                                    new XAttribute("Name",dep0.Name),
            //                                                    new XAttribute("CountOfBooks", dep0.BooksCounter()),
            //                                                    new XElement("Books", new XElement("Book", "gfd"))),
            //                                                new XElement("Department",
            //                                                    new XAttribute("Name", dep1.Name),
            //                                                    new XAttribute("CountOfBooks", dep1.BooksCounter()),
            //                                                    new XElement("Books", dep1.Books)),
            //                                                new XElement("Department",
            //                                                    new XAttribute("Name", dep2.Name),
            //                                                    new XAttribute("CountOfBooks", dep2.BooksCounter()),
            //                                                    new XElement("Books", dep2.Books)),
            //                                                new XElement("Department",
            //                                                    new XAttribute("Name", dep3.Name),
            //                                                    new XAttribute("CountOfBooks", dep3.BooksCounter()),
            //                                                    new XElement("Books", dep3.Books))));
            //doc.Save("doc.xml");
        }
    }
}
