﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{
    
    [XmlInclude(type: typeof(Departments))]
    public abstract class Departments : IComparable, ICountingBooks
    {
        public string Name { get; set; }  
        public List<Book> Books { get; set; }
        public abstract int BooksCounter();
        public int CompareTo(object Departmnt)
        {
            Departments obj = Departmnt as Departments;
            return this.Books.Count.CompareTo(obj.Books.Count);
        }
        public void showBooks()
        {
            Console.Clear();
            Console.WriteLine(this.Name);
            Console.WriteLine("----------------------------------");
            foreach (Book book in Books)
            {
                Console.WriteLine(book.ToString());
                Console.WriteLine("\t" + book.Author.ToString());
            }
        }

    }
}
