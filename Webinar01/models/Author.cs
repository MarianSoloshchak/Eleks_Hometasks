﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webinar01
{
    public class Author : IComparable, ICountingBooks
    {
        
        public string Name { get; set; }
        private List<Book> books;
        public List<Book> Books
        {
            get
            {
                if (books != null)
                {
                    return books;
                }
                else
                {
                    Books = new List<Book>();
                    return books;
                }

            }
            set
            {
                books = value;
            }
        }
        public Author()
        {
            Name = "Default Author";
        }
        public Author(string name)
        {
            Name = name;
        }
        public int CompareTo(object obj)
        {
            Author author = obj as Author;
            return this.Books.Count.CompareTo(author.Books.Count);
        }
        public int BooksCounter()
        {
            return this.Books.Count;
        }
        public Author setBook(Book book)
        {
            this.Books.Add(book);
            return this;
        }
        public override string ToString()
        {
            return this.Name + ". Count of Books: " + this.BooksCounter();
        }
    }
}
