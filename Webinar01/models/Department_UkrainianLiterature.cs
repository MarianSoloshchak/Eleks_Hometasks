﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{
    [XmlInclude(typeof(Department_UkrainianLiterature))]
    public class Department_UkrainianLiterature : Departments
    {

        public Department_UkrainianLiterature()
        {
            Name = "Department of Ukrainian Literature";
        }
        public Department_UkrainianLiterature(List<Book> b)
        {
            Name = "Department of Ukrainian Literature";
            Books = b;
        }
        public override string ToString()
        {
            return Name + ": Count of Books - " + this.Books.Count;
        }
        public override int BooksCounter()
        {
            return this.Books.Count;
        }
    }
}
