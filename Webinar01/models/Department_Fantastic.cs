﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webinar01
{ 
    [XmlInclude(typeof(Department_Fantastic))]
    public class Department_Fantastic : Departments
    {

        public Department_Fantastic()
        {
            Name = "Department of fantastic";
        }
        public Department_Fantastic(List<Book> b)
        {
            Name = "Department of fantastic";
            Books = b;
        }
        public override string ToString()
        {
            return Name;
        }
        public override int BooksCounter()
        {
            return this.Books.Count;
        }
    }
}
