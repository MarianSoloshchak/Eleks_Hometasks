﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Webinar01
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library("Stefanyka");
            bool flag = true;
            ConsoleKeyInfo key;
            while(flag)
            {
                Console.Clear();
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("\t\t\t\t LIBRARY");
                Console.WriteLine("-------------------------------------------------------------------");
                int i = 1;
                foreach (var obj in library.Departments)
                {
                    Console.WriteLine("{0}) {1}", i, obj.ToString());
                    i++;
                }
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("If you want to close program press \'q\'");
                key = Console.ReadKey();
                switch (key.KeyChar)
                {
                    case '1':
                        library.Departments[0].showBooks();
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("Enter 1 if you want go back to menu, or 0 if you want to close program");
                        Console.WriteLine("----------------------------------");
                        ConsoleKeyInfo exit = Console.ReadKey();
                        if(exit.KeyChar=='0')
                        {
                            flag = false;
                        }
                        break;
                    case '2':
                        library.Departments[1].showBooks();
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("Enter 1 if you want go back to menu, or 0 if you want to close program");
                        Console.WriteLine("----------------------------------");
                        exit = Console.ReadKey();
                        if (exit.KeyChar == '0')
                        {
                            flag = false;
                        }
                        break;
                    case '3':
                        library.Departments[2].showBooks();
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("Enter 1 if you want go back to menu, or 0 if you want to close program");
                        Console.WriteLine("----------------------------------");
                        exit = Console.ReadKey();
                        if (exit.KeyChar == '0')
                        {
                            flag = false;
                        }
                        break;
                    case '4':
                        library.Departments[3].showBooks();
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("Enter 1 if you want go back to menu, or 0 if you want to close program");
                        Console.WriteLine("----------------------------------");
                        exit = Console.ReadKey();
                        if (exit.KeyChar == '0')
                        {
                            flag = false;
                        }
                        break;
                    case 'q':
                        flag = false;
                        break;
                    default:
                        break;
                }
                library.toXML();
                
                //foreach(var obj in library.departments)
                //{
                //    Console.WriteLine("{0}) {1}",i , obj.ToString());
                //    Departments dep = obj as Departments;
                //    foreach(var book in dep.Books)
                //    {
                //        Console.WriteLine("-------------------------------------------------------------------");
                //        Console.WriteLine("\t " + book.ToString());
                //        Console.WriteLine("\t\t " + book.Author.ToString());
                //        Console.WriteLine("-------------------------------------------------------------------");
                //    }
                //    i++;
                //}
                Console.WriteLine();
              
            }
           
            
        }
    }
}
